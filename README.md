#Test Automation Assessment

Tech Stack Used : 
C#.Net
Visual studio community version 2019
The framework used for BBD is Cucumber for .Net - specflow
unit test framework : NUnit
Selenium WebDriver


#The following nugget packages will be required to execute tests: 
Install-Package Specflow (for BDD) |
Install-Package SpecFlow.NUnit     |
Install-Package NUnit - (unit testing framework) |
Install-Package NUnit.Console |
Install-Package Selenium.WebDriver      |
Install-Package Selenium.Chrome.WebDriver |
Microsoft.NET.Test.Sdk     |
SpecFlow.Tools.MsBuild.Generation  |
RestSharp - (library for automating API's)  |
ExtentReport.Core - (library for Test Reporting)  |
Install Newtonsoft.Json;  |
Also Install Specflow For Visual Studio under Extensions. |
Make sure the Chrome  version on your local machine is  85



# InspiredTestingAssessment
 - This Page Object Model is Object Oriented Programming driven.
 - There are 3 Core Classes from ReusableCoreFunctions folder : for code cleanness and easy maintanability
   WebCoreReusable Class > This consist of all re-usable functions that are used across the framework, so all attributes and behaviour of this class will be accessible across Pagefunctions classes.
   BasePage Class > This is an abstract class that will inherit the behaviour of BrowserablePage below 
   BrowserablePage Class > This is an Abstract Class that will get and set the url string for the application under test
 - Each webpage to be automated is represented as  PageClass and PageFunctionsClass where PageFunctionsClass is inheriting from PageClass for code cleanness and easy maintanability.
 - PageClass e.g.: DepositOnFirstAccountPage is storing all web elements that were found via selenium locators for Deposit user journey
 - PageFunctionsCLass e.g.:DepositOnFirstAccountPageFunctions It consist of the functions applicable for cash deposit on first account and its inheriting from DepositOnFirstAccountPage Class.
 - Some other tests requires test data in order to run, as per design data resides on feature files,and it passed through step definition Class to specfic functions where 
   its passed as parameters then Page Objects Class where it will be mapped with webElements Until to the web where it has to be populated.  
 
# This is a Behaviour Driven Development Framework:
  - Test Data is stored on feature files from specflow or Cucumber e.g.: For a deposit cash tests Amounts to be deposited are stored on a feature file.
  - Parameterization has been achieved through feature files so that the script can be executed multiple times for different set of data eg:
    Deposit cash script can be executed multiple times creating more cash deposit , one after the other. 

Unit Test Framework :
  - Assertions from unit test framework - Nunit has been used to force failures on tests where applicable.
  - Unit test framework has been used for test execution as well.

 
#Framework Organization/Structure:
The framework consists of the folders :
 - CoreReusableFunctions - This folder consists of classes with reusable functions
 - FeatureFiles - This consists all feature files for test scenarios and storing of test data. 
 - PageFunctions - This consists of all Page Functions Classses.
 - PageObjects - This consists of all Page Object Classses.

# To Execute Tests:
 - Build Solution - there shoud be no errors
 - Navigate to Test Menu on Visual Studio and Open Test Explorer
 - You should be able to locate automated tests as per feature files then Right-Click and Run. 

#Screenshots are saved on \\InspiredTestAssessment\bin\Debug\netcoreapp3.1
# Reports will be saved on file:///C:/Users/XXXXXXXX/source/repos/Result/index.html#

 Please Note: All my automated tests are running 100% smooth see attached screenshots as evidence.

Uploading Assessment on GitLab
